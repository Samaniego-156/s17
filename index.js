
let students = [];
let findcase = [];

function addStudent(student){
	findcase.unshift(student.toLowerCase());
	students.unshift(student);
	console.log(student + " was added to the Students List");
};


function countStudents(){
	console.log("There are a total of " + students.length + " students enrolled.");
};


let sorted = [];

function printStudents(){
	students.sort();
	students.forEach(function(students){
		console.log(students);
	});
}

function findStudent(studentName) {
	let outputNameList = []
	
	students.forEach(student => {
		if (student.toLowerCase().includes(studentName.toLowerCase())) {
			outputNameList.push(student)
		}
	})
	
	let joinedOutputNames = outputNameList.join(", ")
	
	if (outputNameList.length === 1) {
		console.log(`${outputNameList[0]} is an enrollee`)
	} else if (outputNameList.length > 1) {
		console.log(`S{joinedOutputNames} are enrollees`)
	} else {
		console.log(`${studentName} is not an enrollee`)
	}
}

